var express = require("express");
var controller = require("./controller/Controller");
var app = express();

app.get("/", function(req, res) {
  res.send("Hello World!");
});

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*"); // update to match the domain you will make the request from
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});

app.use("/xpto", controller);

app.listen(3000, function() {
  console.log("Example app listening on port 3000!");
});
