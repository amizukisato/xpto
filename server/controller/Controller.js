var express = require("express");
var router = express.Router();
const DAO = require("./../database/DAO");
var dao = new DAO();

class Controller {
  constructor() {
    router.get("/", this.get);
    router.put("/", this.put);
    router.post("/", this.post);
    router.delete("/", this.delete);
  }

  get(req, res) {
    res.send(dao.get());
  }
  put(req, res) {}
  post(req, res) {}
  delete(req, res) {}
}
var controller = new Controller();

module.exports = router;
