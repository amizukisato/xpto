var XPTOModel = require("./XPTOModel");

class DAO {
  constructor() {
    this.runSeed();
  }

  get() {
    return this.database;
  }

  runSeed() {
    this.database = [];

    var estadoMinas = new XPTOModel(
      "Estado de Minas",
      "https://www.em.com.br/",
      "google",
      "angular+"
    );
    var g1 = new XPTOModel(
      "Portal Terra",
      "https://www.terra.com.br/",
      "microsoft",
      "ASP NET"
    );
    var oTempo = new XPTOModel(
      "O Tempo",
      "https://www.otempo.com.br/",
      "google",
      "angularjs"
    );

    var dell = new XPTOModel(
      "DELL",
      "https://dell.com",
      "microsoft",
      "ASP NET"
    );

    var ABCData = new XPTOModel(
      "ABCData",
      "http://localhost:4201",
      "google",
      "angular+"
    );

    this.database.push(g1);
    this.database.push(estadoMinas);
    this.database.push(oTempo);
    this.database.push(dell);
    this.database.push(ABCData);
  }
}

module.exports = DAO;
