# XPTO

XPTO integra diversos sites em uma plataforma única. Com desempenho incrível, você pode agregar todos os seus aplicativos em um a única página


## Instalação

Baixe e instale o nodejs 10+  
```
https://nodejs.org/en/download/
ou 
sudo apt-get install nodejs
```

Após a instalação, instale o angular cli

```
npm install -g @angular/cli
```

Verfique as dependencias, para isso basta rodar o comando npm install
```
cd xpto/server
npm install
```
```
cd xpto/view/xpto-view
npm install
```
```
cd xpto/view/abcdata-view
npm install
```
## Primeira execução

* Inicialize o XPTO Server
```
cd xpto/server
node app.js
```
* Inicialize o XPTO-View
```
cd xpto/view/xpto-view
ng -s
```
* Inicialize o abcdata-view
```
cd xpto/view/abcdata-view
ng -s --port 4201
```



## Running the tests

```
ng -t
```



## Faltando

* Integração com o banco de dados no XPTO
* Integração com server abcdata e abcdata view


## ABCData

Para realizarmos a consolidação dos dados de ABCData executaremos os seguintes passos.

1) Criar uma nova tabela chamada de ProduzidoPlanejado com os campos de volume Produzido e Planejado
2) Nesta tabela adcionar uma nova coluna "categoriaUsuario", que registra o tipo de usuário que possui acesso aos dados
3) Criar um índice invertido para o filtro do dados pelo tipo do usuário
4) Podemos pensar em uma arquitetura que segmenta os dados consolidados, por exemplo  a cada ano.



## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

